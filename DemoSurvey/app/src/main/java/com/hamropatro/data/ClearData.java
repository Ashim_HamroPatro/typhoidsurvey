package com.hamropatro.data;

/**
 * Created by 14 on 2/26/2016.
 */
public class ClearData {

    public static void clearConstant(){

        ComplaintData.txtMainComplaint="";
        ComplaintData.txtDurationIllness="";


        ComplaintData.txtFever="";

        ComplaintData.txtHeadache="";
        ComplaintData.txtAbdominalPain="";
        ComplaintData.txtAnorexia="";
        ComplaintData.txtDiarrhoea="";
        ComplaintData.txtVommiting="";
        ComplaintData.txtConspation="";
        ComplaintData.txtCough="";
        ComplaintData.txtMyalgia="";
        ComplaintData.txtRashes="";
        ComplaintData.txtOthersymptoms="";

        ComplaintData.rdill2months="";
        ComplaintData.txtDuration="";
        ComplaintData.txtChiefSymptoms="";
        ComplaintData.txtTreatmentrecieved="";

        ComplaintData.chkamo="";
        ComplaintData.chkcipro="";
        ComplaintData.chkcotri="";
        ComplaintData.chkofla="";
        ComplaintData.chkcholoram="";
        ComplaintData.chkazithromycin="";
        ComplaintData.chkother2="";
        ComplaintData.txtotherantibiotics="";

        ComplaintData.txtdoseandduration="";

        ComplaintData.rdtyphoidVaccaine="";
        ComplaintData.rdpreviousvaccaine="";
        ComplaintData.familymembervaccine="";


        //

        GeneralData.studyNo="";
        GeneralData.HospitalNo="";
        GeneralData.name="";
        GeneralData.date="";
        GeneralData.dateofBrith="";
        GeneralData.sex="";
        GeneralData.Occupation="";
        GeneralData.street="";
        GeneralData.area="";
        GeneralData.ward="";
        GeneralData.district="";
        GeneralData.townvillage="";
        GeneralData.phoneNum="";
        GeneralData.address="";

        //

        HealthData.chkQ1supplytap="";
        HealthData.chkQ1waterTanker="";
        HealthData.chkQ1tubewell="";
        HealthData.chkQ1lake="";
        HealthData.chkQ1well="";
        HealthData.chkQ1river="";
        HealthData.chkQ1other="";
        HealthData.Q1other="";

        HealthData.chkQ2boiling="";
        HealthData.chkQ2chlorination="";
        HealthData.chkQ2filtering="";
        HealthData.chkQ2sodis="";
        HealthData.chkQ2other="";
        HealthData.Q2other="";


        HealthData.rdlatrine="";
        HealthData.rdhandwashing="";

        HealthData.chkQ5home="";
        HealthData.chkQ5Cafeteria="";
        HealthData.chkQ5Resturants="";
        HealthData.chkQ5other="";
        HealthData.chkq5othertext="";

        HealthData.chkQ6streetVendors="";
        HealthData.chkQ6party="";
        HealthData.chkQ6Resturants="";
        HealthData.chkQ6none="";
        HealthData.chkQ6Unusual="";
        HealthData.chkQ6other="";
        HealthData.chkQ6othertext="";

        HealthData.numInHouse="";
        HealthData.numInKitchen="";

        //
        PhyExamData.txtWeight="";
        PhyExamData. txttemp="";
        PhyExamData.txthr="";
        PhyExamData.txtrr="";
        PhyExamData.txtbp1="";
        PhyExamData.txtbp2="";
        PhyExamData.txtgcs="";
        PhyExamData.txtmental="";

        PhyExamData.chkpallor="";
        PhyExamData.chkicterus="";
        PhyExamData.chkrash="";
        PhyExamData.chklymphadenopathy="";
        PhyExamData.chkcoges="";

        PhyExamData.txtdesc1="";

        PhyExamData.chkstiffneck="";
        PhyExamData.chkcrackles="";
        PhyExamData.chkwheezes="";
        PhyExamData.chkTender="";
        PhyExamData.chkhepa="";
        PhyExamData.chkspleno="";

        PhyExamData.txtdesc2="";

        PhyExamData.txtdesc3="";

        //


        DiagnosisData.txtpd="";
        DiagnosisData.txtpd2="";
        DiagnosisData.txtpd3="";
        DiagnosisData.txtpd4="";

        DiagnosisData.txtpd5="";

        DiagnosisData.txtpd6="";
        DiagnosisData.txtpd7="";
        DiagnosisData.txtpd9="";

        DiagnosisData.txtpd8="";

        DiagnosisData.txtpd10="";


        DiagnosisData.txtpd100="";


        DiagnosisData.chki1="";


        DiagnosisData.chki3="";


        DiagnosisData.chki4="";


        DiagnosisData.chki5="";


        DiagnosisData.chki6="";



        DiagnosisData.chkc1="";


        DiagnosisData.chkc2="";


        DiagnosisData.chkc3="";

    }
}
