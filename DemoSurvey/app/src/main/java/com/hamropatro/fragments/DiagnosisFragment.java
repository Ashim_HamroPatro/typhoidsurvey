package com.hamropatro.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hamropatro.Listeners.LayoutCheckBoxListener;
import com.hamropatro.data.DiagnosisData;
import com.hamropatro.demosurvey.R;

/**
 * Created by 14 on 2/21/2016.
 */
public class DiagnosisFragment extends Fragment {

    private View mainView;
    private Button btnNext;
    private Button btnPrev;

    private EditText txtpd;
    private EditText txtpd2;
    private EditText txtpd3;
    private EditText txtpd4;

    private EditText txtpd5;

    private EditText txtpd6;
    private EditText txtpd7;
    private EditText txtpd9;

    private EditText txtpd8;

    private EditText txtpd10;


    private EditText txtpd100;

    private CheckBox chki1;
    private LinearLayout layIllCourse;

    private CheckBox chki3;
    private LinearLayout layFailedTreatment;

    private CheckBox chki4;
    private LinearLayout layrelapsed;

    private CheckBox chki5;
    private LinearLayout laylost;

    private CheckBox chki6;
    private LinearLayout layother;


    private CheckBox chkc1;
    private LinearLayout layComplicatedIllness;

    private CheckBox chkc2;
    private LinearLayout layHospitalAddmission;

    private CheckBox chkc3;
    private LinearLayout layfinallyrecovedred;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mainView=inflater.inflate(R.layout.diagnosis_fragment,container,false);
        initView();
        loadQuestion();
        return mainView;
    }
    public void initView(){

        btnNext=(Button)mainView.findViewById(R.id.btndiagnosisNext);
        btnPrev=(Button)mainView.findViewById(R.id.btndiagnosisPrev);
        txtpd=(EditText)mainView.findViewById(R.id.txtpd);
        txtpd2=(EditText)mainView.findViewById(R.id.txtpd2);
        txtpd3=(EditText)mainView.findViewById(R.id.txtpd3);
        txtpd4=(EditText)mainView.findViewById(R.id.txtpd4);
        txtpd5=(EditText)mainView.findViewById(R.id.txtpd5);
        txtpd6=(EditText)mainView.findViewById(R.id.txtpd6);
        txtpd7=(EditText)mainView.findViewById(R.id.txtpd7);
        txtpd9=(EditText)mainView.findViewById(R.id.txtpd9);
        txtpd8=(EditText)mainView.findViewById(R.id.txtpd8);
        txtpd10=(EditText)mainView.findViewById(R.id.txtpd10);
        txtpd100=(EditText)mainView.findViewById(R.id.txtpd100);

        chki1=(CheckBox)mainView.findViewById(R.id.chki1);
        layIllCourse=(LinearLayout)mainView.findViewById(R.id.layIllCourse);
        LayoutCheckBoxListener layoutCheckBoxListener=new LayoutCheckBoxListener(chki1,txtpd4,layIllCourse);
        chki1.setOnClickListener(layoutCheckBoxListener);

        chki3=(CheckBox)mainView.findViewById(R.id.chki3);
        layFailedTreatment=(LinearLayout)mainView.findViewById(R.id.layFailedTreatment);
        layoutCheckBoxListener=new LayoutCheckBoxListener(chki3,txtpd5,layFailedTreatment);
        chki3.setOnClickListener(layoutCheckBoxListener);

        chki4=(CheckBox)mainView.findViewById(R.id.chki4);
        layrelapsed=(LinearLayout)mainView.findViewById(R.id.layrelapsed);
        layoutCheckBoxListener=new LayoutCheckBoxListener(chki4,txtpd6,layrelapsed);
        chki4.setOnClickListener(layoutCheckBoxListener);

        chki5=(CheckBox)mainView.findViewById(R.id.chki5);
        laylost=(LinearLayout)mainView.findViewById(R.id.laylost);
        layoutCheckBoxListener=new LayoutCheckBoxListener(chki5,txtpd7,laylost);
        chki5.setOnClickListener(layoutCheckBoxListener);


        chki6=(CheckBox)mainView.findViewById(R.id.chki6);
        layother=(LinearLayout)mainView.findViewById(R.id.layother);
        layoutCheckBoxListener=new LayoutCheckBoxListener(chki6,txtpd9,layother);
        chki6.setOnClickListener(layoutCheckBoxListener);

        chkc1=(CheckBox)mainView.findViewById(R.id.chkc1);
        layComplicatedIllness=(LinearLayout)mainView.findViewById(R.id.layComplicatedIllness);
        layoutCheckBoxListener=new LayoutCheckBoxListener(chkc1,txtpd8,layComplicatedIllness);
        chkc1.setOnClickListener(layoutCheckBoxListener);

        chkc2=(CheckBox)mainView.findViewById(R.id.chkc2);
        layHospitalAddmission=(LinearLayout)mainView.findViewById(R.id.layHospitalAddmission);
        layoutCheckBoxListener=new LayoutCheckBoxListener(chkc2,txtpd10,layHospitalAddmission);
        chkc2.setOnClickListener(layoutCheckBoxListener);

        chkc3=(CheckBox)mainView.findViewById(R.id.chkc3);
        layfinallyrecovedred=(LinearLayout)mainView.findViewById(R.id.layfinallyrecovedred);
        layoutCheckBoxListener=new LayoutCheckBoxListener(chkc3,txtpd10,layfinallyrecovedred);
        chkc3.setOnClickListener(layoutCheckBoxListener);





        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtpd.getText().length()>0) {
                    saveQuestion();
                    Fragment fragment = new SavetoDbFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.content_frame, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }
                else
                {
                    Toast.makeText(getActivity(),"Please Enter the Primary Diagnosis to continue",Toast.LENGTH_LONG).show();
                }
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveQuestion();
                Fragment fragment= new PhysicalExamFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });




    }


    public void saveQuestion(){

        DiagnosisData.txtpd=txtpd.getText().toString();
        DiagnosisData.txtpd2=txtpd2.getText().toString();
        DiagnosisData.txtpd3=txtpd3.getText().toString();
        DiagnosisData.txtpd4=txtpd4.getText().toString();
        DiagnosisData.txtpd5=txtpd5.getText().toString();
        DiagnosisData.txtpd6=txtpd6.getText().toString();
        DiagnosisData.txtpd7=txtpd7.getText().toString();
        DiagnosisData.txtpd9=txtpd9.getText().toString();
        DiagnosisData.txtpd8=txtpd8.getText().toString();
        DiagnosisData.txtpd10=txtpd10.getText().toString();
        DiagnosisData.txtpd100=txtpd100.getText().toString();

        if(chki1.isChecked()){
            DiagnosisData.chki1=chki1.getText().toString();
        }else {
            DiagnosisData.chki1="";

        }
        if(chki3.isChecked()){
            DiagnosisData.chki3=chki3.getText().toString();
        }else {
            DiagnosisData.chki3="";

        }
        if(chki4.isChecked()){
            DiagnosisData.chki4=chki4.getText().toString();
        }else {

            DiagnosisData.chki4="";


        }
        if(chki5.isChecked()){
            DiagnosisData.chki5=chki5.getText().toString();
        }else {

            DiagnosisData.chki5="";

        }
        if(chki6.isChecked()){
            DiagnosisData.chki6=chki6.getText().toString();
        }else {
            DiagnosisData.chki6="";
        }
        if(chkc1.isChecked()){
            DiagnosisData.chkc1=chkc1.getText().toString();
        }else {
            DiagnosisData.chkc1="";

        }
        if(chkc2.isChecked()){
            DiagnosisData.chkc2=chkc2.getText().toString();
        }else {
            DiagnosisData.chkc2="";

        }
        if(chkc3.isChecked()){
            DiagnosisData.chkc3=chkc3.getText().toString();
        }else {
            DiagnosisData.chkc3="";

        }


    }

    public void loadQuestion(){

        txtpd.setText(DiagnosisData.txtpd);
        txtpd2.setText(DiagnosisData.txtpd2);
        txtpd3.setText(DiagnosisData.txtpd3);
        txtpd4.setText(DiagnosisData.txtpd4);
        txtpd5.setText(DiagnosisData.txtpd5);
        txtpd6.setText(DiagnosisData.txtpd6);
        txtpd7.setText(DiagnosisData.txtpd7);
        txtpd9.setText(DiagnosisData.txtpd9);
        txtpd8.setText(DiagnosisData.txtpd8);
        txtpd10.setText(DiagnosisData.txtpd10);
        txtpd100.setText(DiagnosisData.txtpd100);

        if(chki1.getText().toString().equalsIgnoreCase(DiagnosisData.chki1)){

            chki1.setChecked(true);
            layIllCourse.setVisibility(View.VISIBLE);
        }
        if(chki3.getText().toString().equalsIgnoreCase(DiagnosisData.chki3)){

            chki3.setChecked(true);
            layFailedTreatment.setVisibility(View.VISIBLE);
        }
        if(chki4.getText().toString().equalsIgnoreCase(DiagnosisData.chki4)){

            chki4.setChecked(true);
            layrelapsed.setVisibility(View.VISIBLE);
        }
        if(chki5.getText().toString().equalsIgnoreCase(DiagnosisData.chki5)){

            chki5.setChecked(true);
            laylost.setVisibility(View.VISIBLE);
        }
        if(chki6.getText().toString().equalsIgnoreCase(DiagnosisData.chki6)){

            chki6.setChecked(true);
            layother.setVisibility(View.VISIBLE);
        }
        if(chkc1.getText().toString().equalsIgnoreCase(DiagnosisData.chkc1)){

            chkc1.setChecked(true);
            layComplicatedIllness.setVisibility(View.VISIBLE);
        }
        if(chkc2.getText().toString().equalsIgnoreCase(DiagnosisData.chkc2)){

            chkc2.setChecked(true);
            layHospitalAddmission.setVisibility(View.VISIBLE);
        }
        if(chkc3.getText().toString().equalsIgnoreCase(DiagnosisData.chkc3)){

            chkc3.setChecked(true);
            layfinallyrecovedred.setVisibility(View.VISIBLE);
        }





    }


}
