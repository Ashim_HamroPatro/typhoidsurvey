package com.hamropatro.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.hamropatro.data.PhyExamData;
import com.hamropatro.demosurvey.R;

/**
 * Created by 14 on 2/19/2016.
 */
public class PhysicalExamFragment extends Fragment {


    private View mainView;
    private Button btnNext;
    private Button btnPrev;

    private  EditText txtWeight;
    private EditText txttemp;
    private  EditText txthr;
    private  EditText txtrr;
    private  EditText txtbp1;
    private  EditText txtbp2;
    private  EditText txtgcs;
    private  EditText txtmental;

    private  CheckBox chkpallor;
    private CheckBox chkicterus;
    private  CheckBox chkrash;
    private  CheckBox chklymphadenopathy;
    private  CheckBox chkcoges;

    private  EditText txtdesc1;

    private  CheckBox chkstiffneck;
    private  CheckBox chkcrackles;
    private  CheckBox chkwheezes;
    private  CheckBox chkTender;
    private  CheckBox chkhepa;
    private  CheckBox chkspleno;

    private  EditText txtdesc2;

    private  EditText txtdesc3;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mainView=inflater.inflate(R.layout.physicalexam_fragment,container,false);
        initView();
        loadQuestion();
        return mainView;
    }
    public void initView(){

        btnNext=(Button)mainView.findViewById(R.id.btnPhysicalExamNext);
        btnPrev=(Button)mainView.findViewById(R.id.btnPhysicalExamPrev);

        txtWeight=(EditText)mainView.findViewById(R.id.txtWeight);
        txttemp=(EditText)mainView.findViewById(R.id.txttemp);
        txthr=(EditText)mainView.findViewById(R.id.txthr);
        txtrr=(EditText)mainView.findViewById(R.id.txtrr);
        txtbp1=(EditText)mainView.findViewById(R.id.txtbp1);
        txtbp2=(EditText)mainView.findViewById(R.id.txtbp2);
        txtgcs=(EditText)mainView.findViewById(R.id.txtgcs);
        txtmental=(EditText)mainView.findViewById(R.id.txtmental);

        chkpallor=(CheckBox)mainView.findViewById(R.id.chkpallor);
        chkicterus=(CheckBox)mainView.findViewById(R.id.chkicterus);
        chkrash=(CheckBox)mainView.findViewById(R.id.chkrash);
        chklymphadenopathy=(CheckBox)mainView.findViewById(R.id.chklymphadenopathy);
        chkcoges=(CheckBox)mainView.findViewById(R.id.chkcoges);

        txtdesc1=(EditText)mainView.findViewById(R.id.txtdesc1);

        chkstiffneck=(CheckBox)mainView.findViewById(R.id.chkstiffneck);
        chkcrackles=(CheckBox)mainView.findViewById(R.id.chkcrackles);
        chkwheezes=(CheckBox)mainView.findViewById(R.id.chkwheezes);
        chkTender=(CheckBox)mainView.findViewById(R.id.chkTender);
        chkhepa=(CheckBox)mainView.findViewById(R.id.chkhepa);
        chkspleno=(CheckBox)mainView.findViewById(R.id.chkspleno);

        txtdesc2=(EditText)mainView.findViewById(R.id.txtdesc2);
        txtdesc3=(EditText)mainView.findViewById(R.id.txtdesc3);



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveQuestion();
                Fragment fragment= new DiagnosisFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveQuestion();
                Fragment fragment= new ComplaintSymptomsFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });





    }

    public void loadQuestion(){

        txtWeight.setText(PhyExamData.txtWeight);
        txttemp.setText(PhyExamData.txttemp);
        txthr.setText(PhyExamData.txthr);
        txtrr.setText(PhyExamData.txtrr);
        txtbp1.setText(PhyExamData.txtbp1);
        txtbp2.setText(PhyExamData.txtbp2);
        txtgcs.setText(PhyExamData.txtgcs);
        txtmental.setText(PhyExamData.txtmental);

        if(PhyExamData.chkpallor.equalsIgnoreCase(chkpallor.getText().toString())){
            chkpallor.setChecked(true);
        }
        if(PhyExamData.chkicterus.equalsIgnoreCase(chkicterus.getText().toString())){
            chkicterus.setChecked(true);
        }
        if(PhyExamData.chkrash.equalsIgnoreCase(chkrash.getText().toString())){
            chkrash.setChecked(true);
        }
        if(PhyExamData.chklymphadenopathy.equalsIgnoreCase(chklymphadenopathy.getText().toString())){
            chklymphadenopathy.setChecked(true);
        }
        if(PhyExamData.chkcoges.equalsIgnoreCase(chkcoges.getText().toString())){
            chkcoges.setChecked(true);
        }

        txtdesc1.setText(PhyExamData.txtdesc1);

        if(PhyExamData.chkstiffneck.equalsIgnoreCase(chkstiffneck.getText().toString())){
            chkstiffneck.setChecked(true);
        }
        if(PhyExamData.chkstiffneck.equalsIgnoreCase(chkstiffneck.getText().toString())){
            chkstiffneck.setChecked(true);
        }
        if(PhyExamData.chkcrackles.equalsIgnoreCase(chkcrackles.getText().toString())){
            chkcrackles.setChecked(true);
        }
        if(PhyExamData.chkwheezes.equalsIgnoreCase(chkwheezes.getText().toString())){
            chkwheezes.setChecked(true);
        }
        if(PhyExamData.chkTender.equalsIgnoreCase(chkTender.getText().toString())){
            chkTender.setChecked(true);
        }
        if(PhyExamData.chkhepa.equalsIgnoreCase(chkhepa.getText().toString())){
            chkhepa.setChecked(true);
        }
        if(PhyExamData.chkspleno.equalsIgnoreCase(chkspleno.getText().toString())){
            chkspleno.setChecked(true);
        }

        txtdesc2.setText(PhyExamData.txtdesc2);
        txtdesc3.setText(PhyExamData.txtdesc3);







    }
    public void saveQuestion(){

        PhyExamData.txtWeight=txtWeight.getText().toString();
        PhyExamData.txttemp=txttemp.getText().toString();
        PhyExamData.txthr=txthr.getText().toString();
        PhyExamData.txtrr=txtrr.getText().toString();
        PhyExamData.txtbp1=txtbp1.getText().toString();
        PhyExamData.txtbp2=txtbp2.getText().toString();
        PhyExamData.txtgcs=txtgcs.getText().toString();
        PhyExamData.txtmental=txtmental.getText().toString();


        if(chkpallor.isChecked()){
            PhyExamData.chkpallor=chkpallor.getText().toString();
        }else
        {
            PhyExamData.chkpallor="";
        }
        if(chkicterus.isChecked()){
            PhyExamData.chkicterus=chkicterus.getText().toString();
        }else
        {
            PhyExamData.chkicterus="";
        }
        if(chkrash.isChecked()){
            PhyExamData.chkrash=chkrash.getText().toString();
        }else
        {
            PhyExamData.chkrash="";
        }
        if(chklymphadenopathy.isChecked()){
            PhyExamData.chklymphadenopathy=chklymphadenopathy.getText().toString();
        }else
        {
            PhyExamData.chklymphadenopathy="";
        }
        if(chkcoges.isChecked()){
            PhyExamData.chkcoges=chkcoges.getText().toString();
        }else
        {
            PhyExamData.chkcoges="";
        }

        PhyExamData.txtdesc1=txtdesc1.getText().toString();

        if(chkstiffneck.isChecked()){
            PhyExamData.chkstiffneck=chkstiffneck.getText().toString();
        }else
        {
            PhyExamData.chkstiffneck="";
        }

        if(chkcrackles.isChecked()){
            PhyExamData.chkcrackles=chkcrackles.getText().toString();
        }else
        {
            PhyExamData.chkcrackles="";
        }

        if(chkwheezes.isChecked()){
            PhyExamData.chkwheezes=chkwheezes.getText().toString();
        }else
        {
            PhyExamData.chkwheezes="";
        }

        if(chkTender.isChecked()){
            PhyExamData.chkTender=chkTender.getText().toString();
        }else
        {
            PhyExamData.chkTender="";
        }

        if(chkhepa.isChecked()){
            PhyExamData.chkhepa=chkhepa.getText().toString();
        }else
        {
            PhyExamData.chkhepa="";
        }

        if(chkspleno.isChecked()){
            PhyExamData.chkspleno=chkspleno.getText().toString();
        }else
        {
            PhyExamData.chkspleno="";
        }


        PhyExamData.txtdesc2=txtdesc2.getText().toString();

        PhyExamData.txtdesc3=txtdesc3.getText().toString();


    }
}
