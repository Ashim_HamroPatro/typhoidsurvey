package com.hamropatro.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.hamropatro.data.ClearData;
import com.hamropatro.demosurvey.Dashboard;
import com.hamropatro.demosurvey.LauncherActivity;
import com.hamropatro.demosurvey.R;

/**
 * Created by 14 on 2/21/2016.
 */
public class SavetoDbFragment extends Fragment {

    private View mainView;
    private Button btnSaveToDb;
    private Button btnPrev;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mainView=inflater.inflate(R.layout.savetodbfargment,container,false);
        initView();
        return mainView;
    }
    public void initView(){

        btnSaveToDb=(Button)mainView.findViewById(R.id.btnSave);
        btnPrev=(Button)mainView.findViewById(R.id.btnSPrev);
        btnSaveToDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearData.clearConstant();
                Toast.makeText(getActivity(), "Record Saved to Mobile Database", Toast.LENGTH_LONG).show();
               Intent i=new Intent(getActivity(), LauncherActivity.class);
                startActivity(i);

            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment= new DiagnosisFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });




    }
}
