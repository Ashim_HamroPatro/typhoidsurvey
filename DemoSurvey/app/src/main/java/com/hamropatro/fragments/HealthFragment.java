package com.hamropatro.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

import com.hamropatro.Listeners.OtherListener;
import com.hamropatro.data.HealthData;
import com.hamropatro.demosurvey.R;

/**
 * Created by 14 on 2/19/2016.
 */
public class HealthFragment extends Fragment {

    private View mainView;
    private Button btnNext;
    private Button btnPrev;

    private CheckBox chkQ1supplytap;
    private CheckBox chkQ1waterTanker;
    private CheckBox chkQ1tubewell;
    private CheckBox chkQ1lake;
    private CheckBox chkQ1well;
    private CheckBox chkQ1river;
    private CheckBox checkBox1;
    private EditText editText1;

    private CheckBox chkQ2boiling;
    private CheckBox chkQ2chlorination;
    private CheckBox chkQ2filtering;
    private CheckBox chkQ2sodis;
    private CheckBox checkBox2;
    private EditText editText2;

    private RadioButton rdLatrineSanitory;
    private RadioButton rdLatringUnsaitory;
    private RadioButton rdUnavailable;

    private RadioButton rdhandwashingNone;
    private RadioButton rdwaterOnly;
    private RadioButton rdsoap;

    private CheckBox chkQ5Home;
    private CheckBox chkQ5cafetria;
    private CheckBox chkQ5Resturants;
    private  CheckBox checkBox3;
    private EditText editText3;

    private CheckBox chkQ6streetVendors;
    private CheckBox chkQ6party;
    private CheckBox chkQ6Resturants;
    private CheckBox chkQ6none;
    private CheckBox chkQ6Unusual;
    private CheckBox checkBox4;
    private EditText editText4;

    private EditText numberInHouse;
    private EditText numberInKitchen;





    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mainView=inflater.inflate(R.layout.health_fragment,container,false);
            initView();
        loadQuestion();
        return mainView;
    }
    public void initView(){
        btnNext=(Button)mainView.findViewById(R.id.btnHealthNext);
        btnPrev=(Button)mainView.findViewById(R.id.btnHealthPrev);

        chkQ1supplytap=(CheckBox)mainView.findViewById(R.id.chkSupplyTap);
        chkQ1waterTanker=(CheckBox)mainView.findViewById(R.id.chkWaterTanker);
        chkQ1tubewell=(CheckBox)mainView.findViewById(R.id.chkTubeWell);
        chkQ1lake=(CheckBox)mainView.findViewById(R.id.chkLake);
        chkQ1well=(CheckBox)mainView.findViewById(R.id.chkWell);
        chkQ1river=(CheckBox)mainView.findViewById(R.id.chkRiver);
        checkBox1=(CheckBox)mainView.findViewById(R.id.chkOther);
        editText1=(EditText)mainView.findViewById(R.id.txtOtherwaterSource);

        chkQ2boiling=(CheckBox)mainView.findViewById(R.id.chkBoiling);
        chkQ2chlorination=(CheckBox)mainView.findViewById(R.id.chkChloronitation);
        chkQ2filtering=(CheckBox)mainView.findViewById(R.id.chkFiltering);
        chkQ2sodis=(CheckBox)mainView.findViewById(R.id.chkSodis);
        checkBox2=(CheckBox)mainView.findViewById(R.id.chkother1);
        editText2=(EditText)mainView.findViewById(R.id.txtOtherwaterTreatment);

        rdLatrineSanitory=(RadioButton)mainView.findViewById(R.id.rdSanitary);
        rdLatringUnsaitory=(RadioButton)mainView.findViewById(R.id.rdUnSanitary);
        rdUnavailable=(RadioButton)mainView.findViewById(R.id.rdUnavailable);


        rdhandwashingNone=(RadioButton)mainView.findViewById(R.id.rdnone);
        rdwaterOnly=(RadioButton)mainView.findViewById(R.id.rdWaterOnly);
        rdsoap=(RadioButton)mainView.findViewById(R.id.rdSoap);

        chkQ5Home=(CheckBox)mainView.findViewById(R.id.chkHome);
        chkQ5cafetria=(CheckBox)mainView.findViewById(R.id.chkCafeteria);
        chkQ5Resturants=(CheckBox)mainView.findViewById(R.id.chkResturant);
        checkBox3=(CheckBox)mainView.findViewById(R.id.chkEatingPlaceOther);
        editText3=(EditText)mainView.findViewById(R.id.txtOtherEatingPlace);


        chkQ6streetVendors=(CheckBox)mainView.findViewById(R.id.chkStreetVendors);
        chkQ6party=(CheckBox)mainView.findViewById(R.id.chkParty);
        chkQ6Resturants=(CheckBox)mainView.findViewById(R.id.chkresturants3weeks);
        chkQ6none=(CheckBox)mainView.findViewById(R.id.chkNone);
        chkQ6Unusual=(CheckBox)mainView.findViewById(R.id.chkUnusual);
        checkBox4=(CheckBox)mainView.findViewById(R.id.chkeatingOther3weeks);
        editText4=(EditText)mainView.findViewById(R.id.txtothereating3weeeks);

        numberInHouse=(EditText)mainView.findViewById(R.id.txtNumberinHouse);
        numberInKitchen=(EditText)mainView.findViewById(R.id.txtNumberinKitchen);










        OtherListener otherListener=new OtherListener(checkBox1,editText1);
        checkBox1.setOnClickListener(otherListener);
        otherListener=new OtherListener(checkBox2,editText2);
        checkBox2.setOnClickListener(otherListener);
        otherListener=new OtherListener(checkBox3,editText3);
        checkBox3.setOnClickListener(otherListener);
        otherListener=new OtherListener(checkBox4,editText4);
        checkBox4.setOnClickListener(otherListener);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveQuestion();
                Fragment fragment= new ComplaintSymptomsFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveQuestion();
                Fragment fragment= new GeneralFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });


    }


    public void loadQuestion(){

        if(HealthData.chkQ1supplytap.equalsIgnoreCase(chkQ1supplytap.getText().toString())){
            chkQ1supplytap.setChecked(true);
        }
        if(HealthData.chkQ1waterTanker.equalsIgnoreCase(chkQ1waterTanker.getText().toString())){
            chkQ1waterTanker.setChecked(true);
        }
        if(HealthData.chkQ1tubewell.equalsIgnoreCase(chkQ1tubewell.getText().toString())){
            chkQ1tubewell.setChecked(true);
        }
        if(HealthData.chkQ1lake.equalsIgnoreCase(chkQ1lake.getText().toString())){
            chkQ1lake.setChecked(true);
        }
        if(HealthData.chkQ1well.equalsIgnoreCase(chkQ1well.getText().toString())){
            chkQ1well.setChecked(true);
        }
        if(HealthData.chkQ1river.equalsIgnoreCase(chkQ1river.getText().toString())){
            chkQ1river.setChecked(true);
        }
        if(HealthData.chkQ1other.equalsIgnoreCase(checkBox1.getText().toString())){
            checkBox1.setChecked(true);
            editText1.setVisibility(View.VISIBLE);
            editText1.setText(HealthData.Q1other);
        }

        ///Water treatment

        if(HealthData.chkQ2boiling.equalsIgnoreCase(chkQ2boiling.getText().toString())){
            chkQ2boiling.setChecked(true);
        }
        if(HealthData.chkQ2chlorination.equalsIgnoreCase(chkQ2chlorination.getText().toString())){
            chkQ2chlorination.setChecked(true);
        }
        if(HealthData.chkQ2filtering.equalsIgnoreCase(chkQ2filtering.getText().toString())){
            chkQ2filtering.setChecked(true);
        }
        if(HealthData.chkQ2sodis.equalsIgnoreCase(chkQ2sodis.getText().toString())){
            chkQ2sodis.setChecked(true);
        }
        if(HealthData.chkQ2other.equalsIgnoreCase(checkBox2.getText().toString())){
            checkBox2.setChecked(true);
            editText2.setVisibility(View.VISIBLE);
            editText2.setText(HealthData.Q2other);
        }


        //
        if(rdLatrineSanitory.getText().toString().equalsIgnoreCase(HealthData.rdlatrine)){
            rdLatrineSanitory.setChecked(true);
        }else
        if(rdLatringUnsaitory.getText().toString().equalsIgnoreCase(HealthData.rdlatrine)){
            rdLatringUnsaitory.setChecked(true);
        }else
        if(rdUnavailable.getText().toString().equalsIgnoreCase(HealthData.rdlatrine)){
            rdUnavailable.setChecked(true);
        }

        //
        if(rdhandwashingNone.getText().toString().equalsIgnoreCase(HealthData.rdhandwashing)){
            rdhandwashingNone.setChecked(true);
        }else
        if(rdwaterOnly.getText().toString().equalsIgnoreCase(HealthData.rdhandwashing)){
            rdwaterOnly.setChecked(true);
        }else
        if(rdsoap.getText().toString().equalsIgnoreCase(HealthData.rdhandwashing)){
            rdsoap.setChecked(true);
        }
        //

        if(HealthData.chkQ5home.equalsIgnoreCase(chkQ5Home.getText().toString())){
            chkQ5Home.setChecked(true);
        }
        if(HealthData.chkQ5Cafeteria.equalsIgnoreCase(chkQ5cafetria.getText().toString())){
            chkQ5cafetria.setChecked(true);
        }
        if(HealthData.chkQ5Resturants.equalsIgnoreCase(chkQ5Resturants.getText().toString())){
            chkQ5Resturants.setChecked(true);
        }
        if(HealthData.chkQ5other.equalsIgnoreCase(checkBox3.getText().toString())){
            checkBox3.setChecked(true);
            editText3.setVisibility(View.VISIBLE);
            editText3.setText(HealthData.chkq5othertext);
        }


        //

        if(HealthData.chkQ6streetVendors.equalsIgnoreCase(chkQ6streetVendors.getText().toString())){
            chkQ6streetVendors.setChecked(true);
        }
        if(HealthData.chkQ6party.equalsIgnoreCase(chkQ6party.getText().toString())){
            chkQ6party.setChecked(true);
        }
        if(HealthData.chkQ6Resturants.equalsIgnoreCase(chkQ6Resturants.getText().toString())){
            chkQ6Resturants.setChecked(true);
        }
        if(HealthData.chkQ6none.equalsIgnoreCase(chkQ6none.getText().toString())){
            chkQ6none.setChecked(true);
        }
        if(HealthData.chkQ6Unusual.equalsIgnoreCase(chkQ6Unusual.getText().toString())){
            chkQ6Unusual.setChecked(true);
        }
        if(HealthData.chkQ6other.equalsIgnoreCase(checkBox4.getText().toString())){
            checkBox4.setChecked(true);
            editText4.setVisibility(View.VISIBLE);
            editText4.setText(HealthData.chkQ6othertext);
        }

        numberInHouse.setText(HealthData.numInHouse);
        numberInKitchen.setText(HealthData.numInKitchen);

















    }




    public void saveQuestion(){

        if(chkQ1supplytap.isChecked()){
            HealthData.chkQ1supplytap=chkQ1supplytap.getText().toString();
        }else{
            HealthData.chkQ1supplytap="";
        }
        if(chkQ1waterTanker.isChecked()){
            HealthData.chkQ1waterTanker=chkQ1waterTanker.getText().toString();
        }else{
            HealthData.chkQ1waterTanker="";
        }
        if(chkQ1tubewell.isChecked()){
            HealthData.chkQ1tubewell=chkQ1tubewell.getText().toString();
        }else{
            HealthData.chkQ1tubewell="";
        }
        if(chkQ1lake.isChecked()){
            HealthData.chkQ1lake=chkQ1lake.getText().toString();
        }else{
            HealthData.chkQ1lake="";
        }
        if(chkQ1well.isChecked()){
            HealthData.chkQ1well=chkQ1well.getText().toString();
        }else{
            HealthData.chkQ1well="";
        }
        if(chkQ1river.isChecked()){
            HealthData.chkQ1river=chkQ1river.getText().toString();
        }else{
            HealthData.chkQ1river="";
        }
        if(checkBox1.isChecked()){

            HealthData.chkQ1other=checkBox1.getText().toString();
            HealthData.Q1other=editText1.getText().toString();
        }else
        {
            HealthData.chkQ1other="";
            HealthData.Q1other="";
        }

        if(chkQ2boiling.isChecked()){
            HealthData.chkQ2boiling=chkQ2boiling.getText().toString();
        }else{
            HealthData.chkQ2boiling="";
        }
        if(chkQ2chlorination.isChecked()){
            HealthData.chkQ2chlorination=chkQ2chlorination.getText().toString();
        }else{
            HealthData.chkQ2chlorination="";
        }
        if(chkQ2filtering.isChecked()){
            HealthData.chkQ2filtering=chkQ2filtering.getText().toString();
        }else{
            HealthData.chkQ2filtering="";
        }
        if(chkQ2sodis.isChecked()){
            HealthData.chkQ2sodis=chkQ2sodis.getText().toString();
        }else{
            HealthData.chkQ2sodis="";
        }
        if(checkBox2.isChecked()){
            HealthData.chkQ2other=checkBox2.getText().toString();
            HealthData.Q2other=editText2.getText().toString();
        }else{
            HealthData.chkQ2other="";
            HealthData.Q2other="";
        }

        if(rdLatrineSanitory.isChecked()){
            HealthData.rdlatrine=rdLatrineSanitory.getText().toString();

        }else if(rdLatringUnsaitory.isChecked()){
            HealthData.rdlatrine=rdLatringUnsaitory.getText().toString();

        }else if(rdUnavailable.isChecked()){
            HealthData.rdlatrine=rdUnavailable.getText().toString();

        }

        if(rdhandwashingNone.isChecked()){
           HealthData.rdhandwashing=rdhandwashingNone.getText().toString();

        }else if(rdwaterOnly.isChecked()){
            HealthData.rdhandwashing=rdwaterOnly.getText().toString();

        }else if(rdsoap.isChecked()){
            HealthData.rdhandwashing=rdsoap.getText().toString();

        }

        if(chkQ5Home.isChecked()){
            HealthData.chkQ5home=chkQ5Home.getText().toString();
        }else{
            HealthData.chkQ5home="";
        }

        if(chkQ5cafetria.isChecked()){
            HealthData.chkQ5Cafeteria=chkQ5cafetria.getText().toString();
        }else{
            HealthData.chkQ5Cafeteria="";
        }
        if(chkQ5Resturants.isChecked()){
            HealthData.chkQ5Resturants=chkQ5Resturants.getText().toString();
        }else{
            HealthData.chkQ5Resturants="";
        }
        if(checkBox3.isChecked()){
            HealthData.chkQ5other=checkBox3.getText().toString();
            HealthData.chkq5othertext=editText3.getText().toString();
        }else{
            HealthData.chkQ5other="";
            HealthData.chkq5othertext="";
        }

        if(chkQ6streetVendors.isChecked()){
            HealthData.chkQ6streetVendors=chkQ6streetVendors.getText().toString();
        }else{
            HealthData.chkQ6streetVendors="";
        }
        if(chkQ6party.isChecked()){
            HealthData.chkQ6party=chkQ6party.getText().toString();
        }else{
            HealthData.chkQ6party="";
        }
        if(chkQ6Resturants.isChecked()){
            HealthData.chkQ6Resturants=chkQ6Resturants.getText().toString();
        }else{
            HealthData.chkQ6Resturants="";
        }
        if(chkQ6none.isChecked()){
            HealthData.chkQ6none=chkQ6none.getText().toString();
        }else{
            HealthData.chkQ6none="";
        }
        if(chkQ6Unusual.isChecked()){
            HealthData.chkQ6Unusual=chkQ6Unusual.getText().toString();
        }else{
            HealthData.chkQ6Unusual="";
        }
        if(checkBox4.isChecked()){
            HealthData.chkQ6other=checkBox4.getText().toString();
            HealthData.chkQ6othertext=editText4.getText().toString();

        }else{
            HealthData.chkQ6other="";
            HealthData.chkQ6othertext="";
        }

        HealthData.numInHouse=numberInHouse.getText().toString();
        HealthData.numInKitchen=numberInKitchen.getText().toString();

    }


}
