package com.hamropatro.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.hamropatro.Listeners.OtherListener;
import com.hamropatro.Listeners.RadioLayoutListener;
import com.hamropatro.data.ComplaintData;
import com.hamropatro.demosurvey.R;

/**
 * Created by 14 on 2/19/2016.
 */
public class ComplaintSymptomsFragment extends Fragment {

    private View mainView;
    private Button btnNext;
    private Button btnPrev;
    private EditText txtMainComplaint;
    private EditText txtDurationIllness;

    private EditText txtFever;
    private EditText txtHeadache;
    private EditText txtAbdominalPain;
    private EditText txtAnorexia;
    private EditText txtDiarrhoea;
    private EditText txtVommiting;
    private EditText txtConspation;
    private EditText txtCough;
    private EditText txtMyalgia;
    private EditText txtRashes;
    private EditText txtOthersymptoms;

    private RadioButton rdillyes;
    private RadioButton rdillNo;
    private LinearLayout layIll;
    private EditText txtDuration;
    private EditText txtChiefSymptoms;
    private EditText txtTreatmentrecieved;


    private CheckBox chkamo;
    private CheckBox chkcipro;
    private CheckBox chkcotri;
    private CheckBox chkofla;
    private CheckBox chkcholoram;
    private CheckBox chkazithromycin;
    private CheckBox chkother2;
    private EditText txtotherantibiotics;

    private RadioButton rdyest1;
    private RadioButton rdnot1;
    private DatePicker dttmt1;

    private RadioButton rdyest2;
    private RadioButton rdnot2;
    private DatePicker dttmt2;

    private RadioButton rdyest3;
    private RadioButton rdnot3;
    private DatePicker dttmt3;

    private EditText txtdoseandduration;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mainView=inflater.inflate(R.layout.symptoms_fragment,container,false);
        initView();
        loadQuestion();
        return mainView;
    }
    public void initView(){

            btnNext=(Button)mainView.findViewById(R.id.btnSymptomsNext);
            btnPrev=(Button)mainView.findViewById(R.id.btnSymptomsPrev);


        txtMainComplaint=(EditText)mainView.findViewById(R.id.txtMainComplaint);
        txtDurationIllness=(EditText)mainView.findViewById(R.id.txtDurationIllness);
        txtFever=(EditText)mainView.findViewById(R.id.txtFever);
        txtHeadache=(EditText)mainView.findViewById(R.id.txtHeadache);
        txtAbdominalPain=(EditText)mainView.findViewById(R.id.txtAbdominalPain);
        txtAnorexia=(EditText)mainView.findViewById(R.id.txtAnorexia);
        txtDiarrhoea=(EditText)mainView.findViewById(R.id.txtDiarrhoea);
        txtVommiting=(EditText)mainView.findViewById(R.id.txtVommiting);
        txtConspation=(EditText)mainView.findViewById(R.id.txtConspation);
        txtCough=(EditText)mainView.findViewById(R.id.txtCough);
        txtMyalgia=(EditText)mainView.findViewById(R.id.txtMyalgia);
        txtRashes=(EditText)mainView.findViewById(R.id.txtRashes);
        txtOthersymptoms=(EditText)mainView.findViewById(R.id.txtOthersymptoms);
        rdillyes=(RadioButton)mainView.findViewById(R.id.rdillYes);
        rdillNo=(RadioButton)mainView.findViewById(R.id.rdillNo);
        layIll=(LinearLayout)mainView.findViewById(R.id.layIll);
        txtDuration=(EditText)mainView.findViewById(R.id.txtDuration);
        txtChiefSymptoms=(EditText)mainView.findViewById(R.id.txtChiefSymptoms);
        txtTreatmentrecieved=(EditText)mainView.findViewById(R.id.txtTreatmentrecieved);
        RadioLayoutListener radioLayoutListener=new RadioLayoutListener(layIll,rdillyes,rdillNo,txtDuration,txtChiefSymptoms,txtTreatmentrecieved);
        rdillyes.setOnClickListener(radioLayoutListener);
        rdillNo.setOnClickListener(radioLayoutListener);

        chkamo=(CheckBox)mainView.findViewById(R.id.chkamo);
        chkcipro=(CheckBox)mainView.findViewById(R.id.chkcipro);
        chkcotri=(CheckBox)mainView.findViewById(R.id.chkcotri);
        chkofla=(CheckBox)mainView.findViewById(R.id.chkofla);
        chkcholoram=(CheckBox)mainView.findViewById(R.id.chkcholoram);
        chkazithromycin=(CheckBox)mainView.findViewById(R.id.chkazithromycin);
        chkother2=(CheckBox)mainView.findViewById(R.id.chkother2);
        txtotherantibiotics=(EditText)mainView.findViewById(R.id.txtotherantibiotics);
        OtherListener otherListener=new OtherListener(chkother2,txtotherantibiotics);
        chkother2.setOnClickListener(otherListener);
        txtdoseandduration=(EditText)mainView.findViewById(R.id.txtdoseandduration);
        rdyest1=(RadioButton)mainView.findViewById(R.id.rdyest1);
        rdnot1=(RadioButton)mainView.findViewById(R.id.rdnot1);
        dttmt1=(DatePicker)mainView.findViewById(R.id.dttmt1);
        radioLayoutListener=new RadioLayoutListener(dttmt1,rdyest1,rdnot1);
        rdyest1.setOnClickListener(radioLayoutListener);
        rdnot1.setOnClickListener(radioLayoutListener);
        rdyest2=(RadioButton)mainView.findViewById(R.id.rdyest2);
        rdnot2=(RadioButton)mainView.findViewById(R.id.rdnot2);
        dttmt2=(DatePicker)mainView.findViewById(R.id.dttmt2);
        radioLayoutListener=new RadioLayoutListener(dttmt2,rdyest2,rdnot2);
        rdyest2.setOnClickListener(radioLayoutListener);
        rdnot2.setOnClickListener(radioLayoutListener);
        rdyest3=(RadioButton)mainView.findViewById(R.id.rdyest3);
        rdnot3=(RadioButton)mainView.findViewById(R.id.rdnot3);
        dttmt3=(DatePicker)mainView.findViewById(R.id.dttmt3);
        radioLayoutListener=new RadioLayoutListener(dttmt3,rdyest3,rdnot3);
        rdyest3.setOnClickListener(radioLayoutListener);
        rdnot3.setOnClickListener(radioLayoutListener);




        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveQuestion();
                Fragment fragment= new PhysicalExamFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveQuestion();
                Fragment fragment= new HealthFragment();
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction().replace(R.id.content_frame,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });


    }

    public void saveQuestion(){

        ComplaintData.txtMainComplaint=txtMainComplaint.getText().toString();
        ComplaintData.txtDurationIllness=txtDurationIllness.getText().toString();
        ComplaintData.txtFever=txtFever.getText().toString();
        ComplaintData.txtHeadache=txtHeadache.getText().toString();
        ComplaintData.txtAbdominalPain=txtAbdominalPain.getText().toString();
        ComplaintData.txtAnorexia=txtAnorexia.getText().toString();
        ComplaintData.txtDiarrhoea=txtDiarrhoea.getText().toString();
        ComplaintData.txtVommiting=txtVommiting.getText().toString();
        ComplaintData.txtConspation=txtConspation.getText().toString();
        ComplaintData.txtCough=txtCough.getText().toString();
        ComplaintData.txtMyalgia=txtMyalgia.getText().toString();
        ComplaintData.txtRashes=txtRashes.getText().toString();
        ComplaintData.txtOthersymptoms=txtOthersymptoms.getText().toString();
        if(rdillyes.isChecked()){
        ComplaintData.rdill2months=rdillyes.getText().toString();
        }else if(rdillNo.isChecked()){
        ComplaintData.rdill2months=rdillNo.getText().toString();
        }
        ComplaintData.txtDuration=txtDuration.getText().toString();
        ComplaintData.txtChiefSymptoms=txtChiefSymptoms.getText().toString();
        ComplaintData.txtTreatmentrecieved=txtTreatmentrecieved.getText().toString();

        if(chkamo.isChecked()){
            ComplaintData.chkamo=chkamo.getText().toString();
        }
        else{
            ComplaintData.chkamo="";
        }
        if(chkcipro.isChecked()){
            ComplaintData.chkcipro=chkcipro.getText().toString();
        }
        else{
            ComplaintData.chkcipro="";
        }
        if(chkcotri.isChecked()){
            ComplaintData.chkcotri=chkcotri.getText().toString();
        }
        else{
            ComplaintData.chkcotri="";
        }
        if(chkofla.isChecked()){
            ComplaintData.chkofla=chkofla.getText().toString();
        }
        else{
            ComplaintData.chkofla="";
        }
        if(chkcholoram.isChecked()){
            ComplaintData.chkcholoram=chkcholoram.getText().toString();
        }
        else{
            ComplaintData.chkcholoram="";
        }
        if(chkazithromycin.isChecked()){
            ComplaintData.chkazithromycin=chkazithromycin.getText().toString();
        }
        else{
            ComplaintData.chkazithromycin="";
        }
        if(chkother2.isChecked()){
            ComplaintData.chkother2=chkother2.getText().toString();
        }
        else{
            ComplaintData.chkother2="";
        }
        ComplaintData.txtotherantibiotics=txtotherantibiotics.getText().toString();
        ComplaintData.txtdoseandduration=txtdoseandduration.getText().toString();

        if(rdyest1.isChecked()){
            ComplaintData.rdtyphoidVaccaine=rdyest1.getText().toString();
        }else if(rdnot1.isChecked()){
            ComplaintData.rdtyphoidVaccaine=rdnot1.getText().toString();

        }

        if(rdyest2.isChecked()){
            ComplaintData.rdpreviousvaccaine=rdyest2.getText().toString();
        }else if(rdnot2.isChecked()){
            ComplaintData.rdpreviousvaccaine=rdnot2.getText().toString();

        }
        if(rdyest3.isChecked()){
            ComplaintData.familymembervaccine=rdyest3.getText().toString();
        }else if(rdnot3.isChecked()){
            ComplaintData.familymembervaccine=rdnot3.getText().toString();

        }

    }


    public void loadQuestion(){

        txtMainComplaint.setText(ComplaintData.txtMainComplaint);
        txtDurationIllness.setText(ComplaintData.txtDurationIllness);
        txtFever.setText(ComplaintData.txtFever);
        txtHeadache.setText(ComplaintData.txtHeadache);
        txtAbdominalPain.setText(ComplaintData.txtAbdominalPain);
        txtAnorexia.setText(ComplaintData.txtAnorexia);
        txtDiarrhoea.setText(ComplaintData.txtDiarrhoea);
        txtVommiting.setText(ComplaintData.txtVommiting);
        txtConspation.setText(ComplaintData.txtConspation);
        txtCough.setText(ComplaintData.txtCough);
        txtMyalgia.setText(ComplaintData.txtMyalgia);
        txtRashes.setText(ComplaintData.txtRashes);
        txtOthersymptoms.setText(ComplaintData.txtOthersymptoms);

        if(rdillyes.getText().toString().equalsIgnoreCase(ComplaintData.rdill2months))
        {
            rdillyes.setChecked(true);
            layIll.setVisibility(View.VISIBLE);
            txtDuration.setText(ComplaintData.txtDuration);
            txtChiefSymptoms.setText(ComplaintData.txtChiefSymptoms);
            txtTreatmentrecieved.setText(ComplaintData.txtTreatmentrecieved);

        }
        else if(rdillNo.getText().toString().equalsIgnoreCase(ComplaintData.rdill2months))
        {
            rdillNo.setChecked(true);
            layIll.setVisibility(View.GONE);

        }

            if(ComplaintData.chkamo.equalsIgnoreCase(chkamo.getText().toString())){
                chkamo.setChecked(true);
            }

        if(ComplaintData.chkcipro.equalsIgnoreCase(chkcipro.getText().toString())){
            chkcipro.setChecked(true);
        }

        if(ComplaintData.chkcotri.equalsIgnoreCase(chkcotri.getText().toString())){
            chkcotri.setChecked(true);
        }

        if(ComplaintData.chkofla.equalsIgnoreCase(chkofla.getText().toString())){
            chkofla.setChecked(true);
        }
        if(ComplaintData.chkcholoram.equalsIgnoreCase(chkcholoram.getText().toString())){
            chkcholoram.setChecked(true);
        }
        if(ComplaintData.chkazithromycin.equalsIgnoreCase(chkazithromycin.getText().toString())){
            chkazithromycin.setChecked(true);
        }
        if(ComplaintData.chkother2.equalsIgnoreCase(chkother2.getText().toString())){
            chkother2.setChecked(true);
            txtotherantibiotics.setVisibility(View.VISIBLE);
            txtotherantibiotics.setText(ComplaintData.txtotherantibiotics);
        }
        txtdoseandduration.setText(ComplaintData.txtdoseandduration);

        if(rdyest1.getText().toString().equalsIgnoreCase(ComplaintData.rdtyphoidVaccaine))
        {
            rdyest1.setChecked(true);
            dttmt1.setVisibility(View.VISIBLE);


        }
        else if(rdnot1.getText().toString().equalsIgnoreCase(ComplaintData.rdtyphoidVaccaine))
        {
            rdnot1.setChecked(true);
            dttmt1.setVisibility(View.GONE);

        }

        if(rdyest2.getText().toString().equalsIgnoreCase(ComplaintData.rdpreviousvaccaine))
        {
            rdyest2.setChecked(true);
            dttmt2.setVisibility(View.VISIBLE);


        }
        else if(rdnot2.getText().toString().equalsIgnoreCase(ComplaintData.rdpreviousvaccaine))
        {
            rdnot2.setChecked(true);
            dttmt2.setVisibility(View.GONE);

        }
        if(rdyest3.getText().toString().equalsIgnoreCase(ComplaintData.familymembervaccine))
        {
            rdyest3.setChecked(true);
            dttmt3.setVisibility(View.VISIBLE);


        }
        else if(rdnot3.getText().toString().equalsIgnoreCase(ComplaintData.familymembervaccine))
        {
            rdnot3.setChecked(true);
            dttmt3.setVisibility(View.GONE);

        }





    }

}
