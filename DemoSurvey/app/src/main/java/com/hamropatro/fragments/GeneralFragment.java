package com.hamropatro.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.hamropatro.data.GeneralData;
import com.hamropatro.demosurvey.R;

/**
 * Created by 14 on 2/19/2016.
 */
public class GeneralFragment extends Fragment {

    private View mainView;
    private Button btnNext;
    private EditText studyNo;
    private EditText HospitalNo;
    private DatePicker date;
    private EditText name;
    private DatePicker dateOfBirth;
    private RadioButton sexmale;
    private RadioButton sexFemale;
    private EditText occupation;
    private EditText street;
    private EditText area;
    private EditText ward;
    private EditText district;
    private EditText townVillage;
    private EditText phonenumber;
    private EditText address;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mainView=inflater.inflate(R.layout.general_fragment,container,false);
        initView();
        loadanswer();
        return mainView;
    }
    public void initView(){

        btnNext=(Button)mainView.findViewById(R.id.btnNext);
        studyNo=(EditText)mainView.findViewById(R.id.txtStudy_no);
          HospitalNo=(EditText)mainView.findViewById(R.id.txtHospitalNo);
          date=(DatePicker)mainView.findViewById(R.id.dttmDate);
          name=(EditText)mainView.findViewById(R.id.txtName);
          dateOfBirth=(DatePicker)mainView.findViewById(R.id.dttmDob);
          sexmale=(RadioButton)mainView.findViewById(R.id.rdMale);
          sexFemale=(RadioButton)mainView.findViewById(R.id.rdFemale);
          occupation=(EditText)mainView.findViewById(R.id.txtOccupation);
          street=(EditText)mainView.findViewById(R.id.txtStreet);
          area=(EditText)mainView.findViewById(R.id.txtArea);
          ward=(EditText)mainView.findViewById(R.id.txtWard);
          district=(EditText)mainView.findViewById(R.id.txtDistrict);
          townVillage=(EditText)mainView.findViewById(R.id.txtTownVill);
          phonenumber=(EditText)mainView.findViewById(R.id.txtPhoneNo);
          address=(EditText)mainView.findViewById(R.id.txtAddress);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new HealthFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                saveQuestion();

            }
        });


    }

    public void saveQuestion(){
        GeneralData.studyNo=studyNo.getText().toString();
        GeneralData.HospitalNo=HospitalNo.getText().toString();
        GeneralData.name=name.getText().toString();
        if(sexmale.isChecked()){
            GeneralData.sex=sexmale.getText().toString();
        }else if(sexFemale.isChecked()) {
            GeneralData.sex = sexFemale.getText().toString();
        }

        GeneralData.Occupation=occupation.getText().toString();
        GeneralData.street=street.getText().toString();
        GeneralData.ward=ward.getText().toString();
        GeneralData.district=district.getText().toString();
        GeneralData.townvillage=townVillage.getText().toString();
        GeneralData.phoneNum=phonenumber.getText().toString();
        GeneralData.address=address.getText().toString();
    }

    public void loadanswer(){

        studyNo.setText(GeneralData.studyNo);
        HospitalNo.setText(GeneralData.HospitalNo);
        name.setText(GeneralData.name);
        if(GeneralData.sex.equalsIgnoreCase(sexmale.getText().toString())){
            sexmale.setChecked(true);
        }else if(GeneralData.sex.equalsIgnoreCase(sexFemale.getText().toString())){
            sexFemale.setChecked(true);
        }
        occupation.setText(GeneralData.Occupation);
        street.setText(GeneralData.street);
        area.setText(GeneralData.area);
        ward.setText(GeneralData.ward);
        district.setText(GeneralData.district);
        townVillage.setText(GeneralData.townvillage);
        phonenumber.setText(GeneralData.phoneNum);
        address.setText(GeneralData.address);


    }


}
