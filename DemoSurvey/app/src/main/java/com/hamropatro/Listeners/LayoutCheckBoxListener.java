package com.hamropatro.Listeners;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by 14 on 2/25/2016.
 */
public class LayoutCheckBoxListener implements View.OnClickListener {


    private CheckBox checkBox;
    private EditText editText;
    private LinearLayout linearLayout;

    public LayoutCheckBoxListener(CheckBox checkBox,EditText editText,LinearLayout linearLayout) {
        this.checkBox=checkBox;
        this.editText=editText;
        this.linearLayout=linearLayout;
    }

    @Override
    public void onClick(View v) {

        if(checkBox.isChecked()){

            linearLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            linearLayout.setVisibility(View.GONE);
            editText.setText("");
        }
    }
}
