package com.hamropatro.Listeners;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Created by 14 on 2/22/2016.
 */
public class OtherListener implements View.OnClickListener {

    private CheckBox checkBox;
    private EditText editText;
    public OtherListener(CheckBox checkBox,EditText editText){
        this.checkBox=checkBox;
        this.editText=editText;

    }

    @Override
    public void onClick(View v) {
        if(checkBox.isChecked()){
            editText.setVisibility(View.VISIBLE);
        }
        else
        {
            editText.setVisibility(View.GONE);
            editText.setText("");
        }
    }
}
