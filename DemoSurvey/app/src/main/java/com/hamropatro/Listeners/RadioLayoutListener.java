package com.hamropatro.Listeners;

import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

/**
 * Created by 14 on 2/22/2016.
 */
public class RadioLayoutListener implements View.OnClickListener {



    private LinearLayout linearLayout;
    private DatePicker datePicker;
    private RadioButton rdyes;
    private RadioButton rdno;
    private Boolean isDate;
    private  EditText editText1;
    private EditText editText2;
    private EditText editText3;
    public RadioLayoutListener(LinearLayout linearLayout,RadioButton rdyes,RadioButton rdno,EditText editText1,EditText editText2,EditText editText3){

        this.linearLayout=linearLayout;
        this.rdyes=rdyes;
        this.rdno=rdno;
        this.editText1=editText1;
        this.editText2=editText2;
        this.editText3=editText3;
        this.isDate=false;
    }
    public RadioLayoutListener(DatePicker datepicker,RadioButton rdyes,RadioButton rdno){

        this.datePicker=datepicker;
        this.rdyes=rdyes;
        this.rdno=rdno;
        this.isDate=true;
    }



    @Override
    public void onClick(View v) {

        if(rdyes.isChecked()){
            if(isDate){
                datePicker.setVisibility(View.VISIBLE);

            }else{
                linearLayout.setVisibility(View.VISIBLE);
            }

        }else if(rdno.isChecked()){

            if(isDate){
                datePicker.setVisibility(View.GONE);

            }else{
                linearLayout.setVisibility(View.GONE);
                editText1.setText("");
                editText2.setText("");
                editText3.setText("");

            }
        }

    }
}
